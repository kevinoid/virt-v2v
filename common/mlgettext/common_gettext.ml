(* This file is generated automatically by ./configure. *)

module Gettext = Gettext.Program (
  struct
    let textdomain = "virt-v2v"
    let codeset = None
    let dir = None
    let dependencies = []
  end
) (GettextStub.Native)
