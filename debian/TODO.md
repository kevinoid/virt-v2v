Debian Packaging To-Do List
===========================

* See if `bundled/libvirt-ocaml` can be removed.
  `docs/virt-v2v-release-notes-1.42.pod` notes it "is bundled for now (it will
  be removed later)".
  Check with upstream.
* `dllv2v_test_harness.so` appears to be statically linked, which causes
  lintian `shared-library-lacks-prerequisites` warning.  Fix it.
* Fix `unstripped-static-library` for `v2v_test_harness.a(v2v_test_harness.o)`
* Is it possible/desirable to build `v2v_test_harness.cmxs` (shared lib) for
  `libv2v-test-harness-ocaml` package?
* Address `source-is-missing test-data/binaries/*`
  As explained in `test-data/binaries/README`, the binaries are generated from
  empty source files.  Is it necessary to cross-compile each of these?
  If so, determine which tests are affected and whether it's worth the effort.
  How does supermin handle this?
* Test building with `Rules-Requires-Root: no` is binary-identical.
* Consider adding `Rules-Requires-Root: dpkg/target-subcommand`
  (and `debhelper/upstream-make-install`?) then adding
  `$(DEB_GAIN_ROOT_CMD) $(MAKE) check-root` to `override_dh_auto_test`
  so that root can be used for check-root in check (as part of build).

  Note: Running `test-v2v-i-ova-as-root.sh` (only check-root test) under
  fakeroot defeats the purpose because the permissions are not really
  set, so nothing is being tested.  `Rules-Requires-Root` may introduce
  compatibility issues?  Does everything support it?
* Add `Build-Depends` on `libvirt-daemon-system` and start daemon before
  `dh_auto_test` so that `test-v2v-o-libvirt.sh` can be run?
* Ship `make check` tests as autopkgtests?  See:
  - https://people.debian.org/~mpitt/autopkgtest/README.package-tests.html
  - https://packaging.ubuntu.com/html/auto-pkg-test.html
  - https://ci.debian.net/doc/
* Is it worth running `$(MAKE) check-slow` in `override_dh_auto_test`?
* Enable CI: https://salsa.debian.org/salsa-ci-team/pipeline#basic-use
* Is there a debhelper autoreconf equivalent for gnulib which runs
  `gnulib-tool` to update bundled gnulib sources?
  Seems like the same trade-offs apply.
