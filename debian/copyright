Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: virt-v2v
Upstream-Contact: libguestfs@redhat.com
Source: http://download.libguestfs.org/virt-v2v/

Files: *
Copyright:
 2007-2015 Richard W.M. Jones, Red Hat Inc.
 2009-2020 Red Hat Inc.
License: GPL-2+

Files: bundled/libvirt-ocaml/*
Copyright: 2007-2015 Richard W.M. Jones, Red Hat Inc.
License: LGPL-2+

Files: debian/*
Copyright: 2020 Kevin Locke <kevin@kevinlocke.name>
License: GPL-2+

Files: gnulib/*
Copyright: 1987-2020 Free Software Foundation, Inc.
License: GPL-3+

Files:
 common/qemuopts/qemuopts.c
 common/qemuopts/qemuopts.h
 common/utils/*.c
 common/utils/*.h
 lib/guestfs-internal-all.h
 run.in
 test-harness/v2v_test_harness.ml
 test-harness/v2v_test_harness.mli
Copyright: 2009-2020 Red Hat Inc.
License: LGPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with the this program; if not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-2+
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with the this program; if not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2 can be found in "/usr/share/common-licenses/LGPL-2".
